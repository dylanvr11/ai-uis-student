---
# Proyectos 2020-2. Inteligencia Artificial. 

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Identificación de fallas en procesos químicos mediante la implementación de técnicas de Machine Learning y Deep Learning](#proy1)
2. [Fly Cancelled](#proy2)
3. [Eliminación de ruido gausiano usando IA.](#proy3)
4. [Happy Mushroom Clasification](#proy4)
---

## Identificación de fallas en procesos químicos mediante la implementación de técnicas de Machine Learning y Deep Learning <a name="proy1"></a>

**Autores: Carlos david Ortega, David Ortiz	**

<img src="https://github.com/cdcandela/AI_project/raw/main/banner_AI.jpeg" style="width:700px;">

**Objetivo: Desarrollar un modelo que calsifique el tipo de falla durante la opreación de un proceso, en este caso el Tennessee Eastman Process**




- Dataset: El dataset cuenta con un total de aproximadamente 15 millones de datos, separados en 4 archivos .RData. Los datasets cuentan con 55 columnas, las cuales representan el código del tipo de falla, enumerado de 0 a 20, y las diferentes variables implicadas en el proceso tales como: temepraturas, presiones, flujos o concentraciones.
- Modelo: DNN, DT


[(code)](https://github.com/cdcandela/AI_project) [(video)](https://www.youtube.com/watch?v=SXTvvqUx8jA) [(+info)](https://github.com/cdcandela/AI_project/blob/main/proyecto_AI.pdf)

---


## Fly Cancelled <a name="proy2"></a>

**Autores: Jorge Andres Burgos, Angel Ronaldo Roa Prada, Einer Steven Jaimes**

<img src="https://gitlab.com/angelrroa/fly-cancelled/-/raw/master/data/Banner_Fly_Cancelled.jpeg" style="width:700px;">

**Objetivo: El objetivo general de este proyecto consiste en analizar la aplicabilidad de las técnicas de clasificación para solucionar los problemas de predicción en la cancelación de los vuelos realizados, en este caso, EEUU.**


- Dataset: January Flight Delay Prediction años 2019-2020
- Modelo: Naive Gaussian Bayes,
Decision Tree Classifier,
Random Forest Classifier,
Deep Neuronal Network,
Support Vector Machines.


[(code)](https://gitlab.com/angelrroa/fly-cancelled) [(video)](https://gitlab.com/angelrroa/fly-cancelled) [(+info)](https://gitlab.com/angelrroa/fly-cancelled/-/blob/master/Presentacion.pptx)

---


## Eliminación de ruido gausiano usando IA. <a name="proy3"></a>

**Autores: Juan Sebastián Estupiñan, Sebastián Rivera León, Juan Sebastián Trujillo Tierradentro**

<img src="https://github.com/Etherion99/Denoising-Project/raw/main/Banner.jpeg" style="width:700px;">

**Objetivo: Modelamiento y entrenamiento de regresores, redes neuronales y redes neuronales convolucionales que permitan corregir las variaciones causadas por el ruido gaussiano en imágenes.**




- Dataset: [MNIST](https://keras.io/api/datasets/mnist/ ), [FASHION MNIST](https://keras.io/api/datasets/fashion_mnist/), [Natural Images](https://www.kaggle.com/prasunroy/natural-images)
- Modelo: Decision Tree Regressors, Support Vector Regressors, Random Forest Regressors, Deep Neural Networks, Convolutional Neural networks


[(code)](https://github.com/Etherion99/Denoising-Project) [(video)](https://www.youtube.com/watch?v=2D00poyd0ok) [(+info)](https://github.com/Etherion99/Denoising-Project/blob/main/Presentaci%C3%B3n.pptx)

---

## Happy Mushroom Clasification <a name="proy4"></a>

**Autores:**
- Isabel Gomez Balvin - 2171895
- Cristhian Diaz Cadena - 2171716
- Jhon Sneider Lopez - 2170098


<img src="https://drive.google.com/file/d/1hES5_iTDKI6QuFUxIbPymazmZtrfUH66/view" style="width:700px;">

[img](https://drive.google.com/file/d/1hES5_iTDKI6QuFUxIbPymazmZtrfUH66/view)
**Objetivo: Ayudar a los exploradores, los biólogos que se mantienen en la naturaleza que en su mayoría cuentan con un celular, y en el caso de encontrarse con un hongo, puedan elegir unas características básicas del hongo y la aplicación pueda clasificarlo como venenoso o comestible**




- Dataset: [mushroom](https://www.kaggle.com/uciml/mushroom-classification)
- Modelo: Clasificador random Forest, Red Neuronal



[(code)](https://github.com/IsabelGomezXx/Happy-Mushroom) [(video)](https://www.youtube.com/watch?v=lSfFKDkXeAo) [(+info)](https://github.com/IsabelGomezXx/Happy-Mushroom)

---

## xx <a name="proy5"></a>

**Autores: xxx	**

<img src="xxx" style="width:700px;">

**Objetivo: xxx**




- Dataset: xxx
- Modelo: xxx


[(code)](xx) [(video)](xx) [(+info)](xxx)

---






