# Inteligencia Artificial I 2021-1

## Bienvenidos!

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/raw/master/imgs/banner_IA.png"  width="1000px" height="200px">


## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb) 

- Necesitas una cuenta de gmail y luego entras a drive
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... gratis! (máximo procesos de 8 horas)
- Con Colaboratory, puedes escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en tu navegador.
- También puedes usar los recursos de computador Local. 


## Calificación
- 40% Talleres
- 30% Parciales
- 30% Proyecto funcional IA 

## Talleres (Problemsets)

Los talleres pretenden ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres ser desarrollará en casa, dentro de las fechas establecidas en el cronograma. 


## Parciales (Quizes)

Son evaluaciones **individuales** basadas en notebooks sobre los temas tratados en las clases. Los estudiantes deben solucionarlo en el salón de clase, en un tiempo determinado. Los apuntes y notebooks del curso se pueden utilizar (curso del repositorio). 


## Proyecto funcional IA

- **Funcionamiento del proyecto**: El proyecto se debe realizar como un notebook y debe ser 100% funcional.

- **Prototipo (PRE-SUS PROJ)**: En este item se considera como esta estructurado el proyecto y se espera una nivel razonable de funcionalidad.

- **Presentación**:
    - Imagen relacionada (800 x 300) con la siguiente información: título del proyecto e información de los estudiantes<br>
    - Video corto (Máximo 5 minutos) (ENTREGAR EL ARCHIVO DE VIDEO y también alojarlo en youtube)<br>
    - Archivo de las diapositivas

- **Sustentación**: Se realizarán preguntas cortas a los estudiantes unicamente relacionadas con el proyecto. 
 
Todos los items tienen el mismo porcentaje de evaluación. 

- **REPOSITORIO DEL PROYECTO**
    - Todos los archivos relacionados con el proyecto deben alojarse en un repositorio de los integrantes del estudainte
    - El archivo readme.md del repositorio debe tener la siguiente información:
        - Titulo del proyecto
        - Banner- Imagen de 800 x 300
        - Autores: Separados por comas
        - Objetivo: Una frase
        - Dataset: información con link de descarga
        - Modelos: Métodos usados para su desarrollo. Escribir solo palabras claves. 
        - Enlaces del código, video, y repositorio
    


**UNICAMENTE SE TENDRAN EN CUENTA LOS PROYECTOS QUE SE HAYAN POSTULADO AL FINALIZAR EL PRIMER CORTE**


## Calendario y plazos

                        SESSION 1            SESSION 2              SESSION SATURDAY


     W01 Nov02-Nov03    Intro                Python-general
     W02 Nov09-Nov10    Python-Numpy         Pandas
     W03 Nov16-Nov17    Pandas               Visualización
     W04 Nov23-Nov24    análisis datos       análisis de datos
     W05 Nov30-Dic01    análisis datos       Aclaraciones	          Parcial 1
     W06 Dic07-Dic08    Intro ML-Gauss       FESTIVO
     W07 Dic14-Dic15    Clasificación A.M.S. Regresión A.M.S.

     ----------------   VACACIONES  DIC 17 - ENE 7 --------------


     W08 Ene11-Ene12    Metricas y valid     Métodos SVM.
     W08 Ene18-Ene19    Métodos Trees.       Deep Learning
     W10 Ene25-Ene26    DNN-imgs-estruc      DNN-audio
     W11 Feb01-Feb02    CNN                  Aclaraciones              parcial 2
     W12 Feb08-Feb09    PRE-SUS PROJ         PRE-SUS PROJ
     W13 Feb15-Feb16    No supervisado       No supervisado
     W14 Feb22-Feb23    Genetic Alg.         intro to  RF.
     W15 Mar01-Mar02    intro to  RF         Aclaraciones              Parcial 3
     W16 Mar08-Mar09    SUS PROJ             SUS PROJ
                  
   

     Ene 14             -> Registro primera nota
     Ene 16             -> Último día cancelación materias
     Mar 11             -> Finalización clase y
     Mar 14 - Mar 18    ->  evaluaciones finales
     Mar 19             -> Registro calificaciones finales
     Mar 22 - Mar 23    -> habilitaciones

    


**ACUERDO n.° 211 DE 2021 - 23 de Julio**
[Calendario academico](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2020/acuerdoAcad434_2020.pdf)


**CUALQUIER ENTREGA FUERA DE PLAZO SERÁ PENALIZADA CON UN 50%**

**LOS PROBLEMSETS ESTAN SUJETOS A CAMBIOS QUE SERÁN DEBIDAMENTE INFORMADOS**

**DEADLINE DE LOS PROBLEMSETS SERÁ EL DIA DE CADA PARCIAL**

