---
# Proyectos 2021-1. Inteligencia Artificial. 

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Clasificador de flores](#proy1)
2. [Desarrollo de un jugador controlado por máquina para videojuegos del género de lucha](#proy2)
3. [Violencia doméstica en Colombia ](#proy3)
4. [Predicción de valores de un stock determinado ](#proy4)
5. [Clasificación de Tumores Cerebrales en imágenes MRI](#proy5)
6. [Algoritmos de regression (ML) para la prediccion del S&P500](#proy6)
7. [Emotion AI](#proy7)
8. [PREVENCION DE ACCIDENTES DE TRANSITO](#proy8)
9. [Clasificación de litologías a partir de registros de pozo](#proy9)
10.[Análisis y seguimiento de delitos cometidos en Bucaramanga](#proy10)
11. [Clasificación de basura](#proy11)
---

## Clasificador de flores<a name="proy1"></a>

**Autores: Julian Camilo Serrano Herrera, Laura Daniela Medina Paipilla**

<img src="https://github.com/lauradanielamedinapa/Proyecto-IA-Clasificador-de-flores/raw/main/flores.png" style="width:700px;">

**Objetivo: Clasificación de imágenes de flores de 5 especies diferentes utilizando modelos de Machine Learning.**

- Dataset: Flowers recognition
- Modelo: Gaussian NB, RandomForestClassifier, DecisionTreeClassifier, SuportVectorClassifier, Red neuronal convolucional.


[(code)](https://github.com/lauradanielamedinapa/Proyecto-IA-Clasificador-de-flores) [(video)](https://youtu.be/ZFet71Q4JeM) [(+info)](https://github.com/lauradanielamedinapa/Proyecto-IA-Clasificador-de-flores)

---
## Desarrollo de un jugador controlado por máquina para videojuegos del género de lucha<a name="proy2"></a>

**Autores: Mateo Flórez Bacca**

<img src="https://gitlab.com/mateoflorezb/ia1ryu/-/raw/main/misc/IA1RyuBanner.png" style="width:700px;">

**Objetivo:Implementar un jugador controlado por máquina para juegos de lucha, basado en machine learning**

- Dataset: Propio. DT Generation/IAR_dataset.csv
- Modelo: DecisionTreeRegressor, SupportVectorRegressor, RandomForestRegressor

[(code)](https://gitlab.com/mateoflorezb/ia1ryu/) [(video)](https://youtu.be/1CREtg24Bj8) [(+info)](https://gitlab.com/mateoflorezb/ia1ryu/)
---
## Violencia doméstica en Colombia <a name="proy3"></a>

**Autores: Ana Gabriela Atuesta Solano - 2162132, Valentina Escobar Bueno - 2180032, Daniel Eduardo González Sánchez -2181932**

<img src="https://github.com/VEEB276/Violencia_Dom-stica_Colombia/blob/main/Presentacion/BANNER%20IAA.jpg?raw=true" style="width:700px;">

**Objetivo:Analizar la tasa de violencia doméstica en Colombia y predecir la fecha de esta.**

- Dataset: https://www.kaggle.com/oscardavidperilla/domestic-violence-in-colombia
- Modelo: DecisionTreeRegressor, RandomForestRegressor.


[(code)](https://github.com/VEEB276/Violencia_Dom-stica_Colombia/tree/main/Notebook) [(video)](https://www.youtube.com/watch?v=0AlVkTLYKFo) [(+info)](https://github.com/VEEB276/Violencia_Dom-stica_Colombia)
---
## Predicción de valores de un stock determinado <a name="proy4"></a>

**Autores: Juan Diaz 2180038, Julián Camacho 2180055**

<img src="https://github.com/Jaerab/pvsd/blob/main/StockPrediction.png?raw=true" style="width:700px;">

**Objetivo: .**

- Dataset: 
- Modelo: 

[(code)]() [(video)]() [(+info)](https://github.com/Jaerab/pvsd)
---
## Clasificación de Tumores Cerebrales en imágenes MRI <a name="proy5"></a>

**Autores: Juan Diego Calderón Carrillo, Daniel Alejandro León Tarazona, Efrain Camilo Quezada Sanchez**

<img src="https://gitlab.com/camilo-quezada/proyecto-ia-clasificacion-de-tumores-cerebrales-en-imagenes-mri/-/raw/main/Data/Logo.jpeg" style="width:700px;">

**Objetivo: Ayudar a pronosticar y clasificar tumores cerebrales**

- Dataset: Brain Tumor Classification MRI
- Modelo: Aprendizaje supervisado,Naive Bayes,DTC,RFC,SVM; Red neuronal convolucional, ResNet50

[(code)](https://gitlab.com/camilo-quezada/proyecto-ia-clasificacion-de-tumores-cerebrales-en-imagenes-mri/-/blob/main/Data/ProyectoIA1.ipynb) [(video)](https://www.youtube.com/watch?v=7g15EKgbXPA) [(+info)](https://gitlab.com/camilo-quezada/proyecto-ia-clasificacion-de-tumores-cerebrales-en-imagenes-mri/-/raw/main/Data/IA1_MRI.pdf?)
---
## Algoritmos de regression (ML) para la prediccion del S&P500 <a name="proy6"></a>

**Autores: Sebastian Pérez López, Edward Andres Sandoval Pineda, Horacio Antonio Camacho Holguín**

<img src="https://raw.githubusercontent.com/Horus19/ProyectoIA/master/banner.jpg" style="width:700px;">

**Objetivo: Estudiar el comportamiento de distintos algoritmos de ML en la tarea de predecir el movimiento relativo del S&P500, tratándolo como un problema autorregresivo.**

- Dataset: Daily last 10 years register S&P500.
- Modelo: Decision Tree Regressor, Random Forest Regressor, Support Vector Regressor, Multilayer Perceptron (Deep Neural Networks)

[(code)](https://github.com/Horus19/ProyectoIA/blob/master/Final_Notebook.ipynb) [(video)](https://www.youtube.com/watch?v=FiUdFWZhLoo) [(+info)](https://github.com/Horus19/ProyectoIA)
---
## Emotion AI <a name="proy7"></a>

**Autores: Jose Luis Soto Soto, Jhon Anderson Ramírez Contreras, Daniel Alejandro Castillo Rodriguez**

<img src="https://gitlab.com/gitlab-god/emotion-ai/-/raw/master/Banner_Emotion_AI.jpg?raw=true" style="width:700px;">

**Objetivo: Identificar las emociones apartir de la imagen de un rostro humano.**

- Dataset: Más de 24000 registros de ubicaciones de pixeles de imagenes de rostros de personas.
- Modelo: Deep Learning, Convolutional Neural Networks, Emotion Detecting

[(code)](https://gitlab.com/gitlab-god/emotion-ai/-/blob/master/Emotion_AI_Final.ipynb) [(video)](https://www.youtube.com/watch?v=F5JlnytKhmc) [(+info)](https://gitlab.com/gitlab-god/emotion-ai)
---

## PREVENCION DE ACCIDENTES DE TRANSITO <a name="proy8"></a>

**Autores: Juan González, Iván Cárdenas, Diego Landinez.**

<img src="https://camo.githubusercontent.com/59e97f83fc34c212ab286895ae9504a0455dffef2724a5d8758c4ebc18e8454f/68747470733a2f2f692e6962622e636f2f4633704c7667462f42616e6e65722e706e67" style="width:700px;">

**Objetivo: Reducir la tasa de accidentabilidad en las vías de medellin mediante el análisis de accidentes pasados..**

- Dataset: Dataset de accidentalidad de la ciudad de Medellín durante el año 2019 Link

- Modelo: NaiveBayes, RandomForest, LinearRegression, K-Means, DecisionTrees, GradientBoostingRegressor.

[(code)](colab.research.google.com/drive/1Zo_UgcdYBBjlQavGjPzolYDv3kUrlICq?usp=sharing) [(video)](youtu.be/FOIzWgpi1Z8) [(+info)](github.com/JUANSSE/IA-1-Project.git)
---

## XXX <a name="proy1"></a>

**Autores: XXXX**

<img src="" style="width:700px;">

**Objetivo: .**

- Dataset: 
- Modelo: 

[(code)]() [(video)]() [(+info)]()
---