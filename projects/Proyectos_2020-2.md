---
# Proyectos 2020-2. Inteligencia Artificial. 

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Clasificación de cuásares, galaxias y estrellas (DR14)](#proy1)
2. [Sistema para la toma de asistencia en la modalidad de presencialidad remota usando reconocimiento facial](#proy2)
3. [Detector de Monedas Inteligente](#proy3)
4. [Aumento de la resolución de imágenes usando redes generativas adversarias](#proy4)
5. [Clasificación de frutas según sus características físicas](#proy5)
6. [Clasificador de insectos: abejas vs avispas](#proy6)
7. [Aplicaciones de la IA en el sector médico](#proy7)
8. [Deep Weather Forecaster](#proy8)
9. [Identificación de vías óptimas para la construcción de ciclorutas](#proy9)
10. [Identificación de enfermedades cardiovasculares riesgosas](#proy10)
11. [Generador FEN de ajedrez](#proy11)
12. [Estudio del Covid19 en Colombia](#proy12)
13. [Dog Breed Classifier](#proy13)
14. [Detección de emociones](#proy14)
15. [Clasificación del grado de Alzheimer mediante imágenes](#proy15)


---

## Clasificación de cuásares, galaxias y estrellas (DR14) <a name="proy1"></a>

**Autores: Andrés Felipe Cabeza, Pablo Eduardo Serrano, Gabriel Felipe Vega**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2162106-2172720-2170122/2162106-2172720-2170122.jpg" style="width:700px;">

**Objetivo: Clasificar entre galaxias, cuásares y estrellas, por medio de los métodos de clasificación tratados en el curso.**

- Dataset: Sloan Digital Sky Survey DR14 (Kaggle)
- Modelo: DNN para clasificación, ML para clasificación (SVC, DecisionTreeClassifier, RandomForestClassifier, GaussianNB, StandardScaler, LabelEncoder).

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2162106-2172720-2170122/2162106-2172720-2170122.ipynb) [(video)](https://www.youtube.com/watch?v=AYKvbrpy5J0) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2162106-2172720-2170122/2162106-2172720-2170122.pdf)-->

---

## Sistema para la toma de asistencia en la modalidad de presencialidad remota usando reconocimiento facial <a name="proy2"></a>

**Autores: Jorge Saul Castillo Jaimes, Angie Loreina García Delgado, Alvaro Luis Rios Garzón**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2111127-2172970-2181705/2111127-2172970-2181705.jpg" style="width:700px;">

**Objetivo: Sistema para la toma de asistencia en la modalidad de presencialidad remota usando reconocimiento facial.**

- Dataset: [(FEI Face Database)](https://fei.edu.br/~cet/facedatabase.html) - Con modificaciones realizadas por los estudiantes.
<!--- Modelo: Red neuronal convolucional, LeNet-->

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2111127-2172970-2181705/2111127-2172970-2181705.ipynb) [(video)](https://www.youtube.com/watch?v=FeDGNOtbUZM) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2111127-2172970-2181705/2111127-2172970-2181705.pdf)--> [(repo)](https://github.com/hopkeinst/AI1-20202/tree/main/Proyecto)

---

## Detector de Monedas Inteligente <a name="proy3"></a>

**Autores: Jose Pumarejo, Neider Smith Narvaez, Luz Mireya Angarita**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2162093-2170128-2162139/2162093-2170128-2162139.jpg" style="width:700px;">

**Objetivo: Clasificar el valor de las monedas por el sonido que hagan al caer sobre una superficie a una distancia x.**

- Dataset: [(coin-dataset)](https://github.com/NeiderSmith/coin-dataset) desarrollado por los estudiantes.
- Modelo: Redes Neuronales, Random Forest, Support Vector Machine, Desicion Tree Clasifier, Naive Bayes.

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2162093-2170128-2162139/2162093-2170128-2162139.ipynb) [(video)](https://www.youtube.com/watch?v=ilsSOJw2V5A) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2162093-2170128-2162139/2162093-2170128-2162139.pdf)-->

---

## Aumento de la resolución de imágenes usando redes generativas adversarias <a name="proy4"></a>

**Autores: Hugo Sebastian Rodriguez Dominguez, Camilo Serrano, Juan Jose Martinez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2171910-2170093-2171458/2171910-2170093-2171458.jpg" style="width:700px;">

**Objetivo: Aumento de la resolución de imágenes usando redes generativas adversarias.**

- Dataset: (...)
<!--- Modelo: DNN para clasificación, ML para clasificación (SVC, DecisionTreeClassifier, RandomForestClassifier, GaussianNB, StandardScaler, LabelEncoder).-->

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2171910-2170093-2171458/2171910-2170093-2171458.ipynb) [(video)](https://www.youtube.com/watch?v=WVYa2YV7ZwI) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2171910-2170093-2171458/2171910-2170093-2171458.pdf)-->

---

## Clasificación de frutas según sus características físicas <a name="proy5"></a>

**Autores: Pedro Alfonso Jimenez B.**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2170082/2170082.jpg" style="width:700px;">

**Objetivo: Clasificación de frutas según sus características físicas.**

- Dataset: [(fruit data with colors)](https://github.com/susanli2016/Machine-Learning-with-Python/blob/master/fruit_data_with_colors.txt)
- Modelo: LogisticRegression, DecisionTreeClassifier, KNeighborsClassifier, LinearDiscriminantAnalysis, GaussianNB y SVC.

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2170082/2170082.ipynb) [(video)](https://www.youtube.com/watch?v=29COn6QlOas) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2170082/2170082.pdf)

---

## Clasificador de insectos: abejas vs avispas <a name="proy6"></a>

**Autores: Jeisson Alejandro Cárdenas Reyes, Geyner Felipe Rojas Torres, Marlon Maurice Mora Carvajal**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2162118-21621087-2171071/2162118-21621087-2171071.jpg" style="width:700px;">

**Objetivo: Clasificar imágenes de abejas y avispas haciendo uso de modelos de machine learning.**

- Dataset:  [(kaggle_bee_vs_wasp)](https://www.kaggle.com/jerzydziewierz/bee-vs-wasp)
- Modelo: Gaussian Naive Bayes, Decision Tree Classifier,  Random Forest Classifier, Support Vector Classifier, Convolutional Neural Network.

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2162118-21621087-2171071/2162118-21621087-2171071.ipynb) [(video)](https://www.youtube.com/watch?v=Dbw4aMoxlRQ) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2152015/2152015.pdf)-->

---

## Aplicaciones de la IA en el sector médico <a name="proy7"></a>

**Autores: Carlos Andres Ochoa Camacho**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2152015/2152015.jpg" style="width:700px;">

**Objetivo: Aplicaciones de la IA en el sector médico.**

- Dataset:  [(Nacidos vivos Bucaramanga)](https://www.datos.gov.co/Salud-y-Protecci-n-Social/Nacidos-Vivos-en-Municipio-de-Bucaramanga-enero-20/x5xp-9w4b)
- Modelo: DecisionTreeRegressor, SVR, neural networks.

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2152015/2152015.ipynb) [(video)](https://www.youtube.com/watch?v=JVZLAteVqD8) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2152015/2152015.pdf)-->

---

## Deep Weather Forecaster <a name="proy8"></a>

**Autores: Brayan Esneider Monroy Chaparro, Geison Alfredo Blanco Rodriguez, Ivan David Ortiz Pineda**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2180034-2180045-2180018/2180034-2180045-2180018.jpg" style="width:700px;">

**Objetivo: Aplicaciones de la IA en el sector médico.**

- Dataset:  [(Jena climate)](https://storage.googleapis.com/tensorflow/tf-keras-datasets/jena_climate_2009_2016.csv.zip)
- Modelo: LSTM, Dense neural network, Convolutional neural network.

[(code)](https://colab.research.google.com/gist/bemc22/fa8bf7cdc634dff78b9a0f90dffeb6a3/deep_weather_forecaster.ipynb) [(video)](https://www.youtube.com/watch?v=mA6vfxTUKo0) [(repo)](https://gitlab.com/proyectoIA/deep-weather-forecaster)

---

## Identificación de vías óptimas para la construcción de ciclorutas <a name="proy9"></a>

**Autores: Andrés Felipe Barajas Welman, Carlos Eduardo Becerra Lizarazo, Oscar Mauricio Campos Sepulveda**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2173039-2171844-2171767/2173039-2171844-2171767.jpg" style="width:700px;">

**Objetivo: Este proyecto tiene como propósito facilitar la identificación de vías óptimas para la construcción de ciclorutas basado en la intensidad vehicular, dada una imagen.**

- Dataset: [(Vehicle Image Database (Universidad politécnica de madrid))](https://www.gti.ssr.upm.es/data/Vehicle_database.html)
- Modelo: Gaussian Naive Bayes, Random Forest Classifier, Decision Tree Classifier, SVC

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2173039-2171844-2171767/2173039-2171844-2171767.ipynb) [(video)](https://www.youtube.com/watch?v=TRRNgzREtX4) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2173039-2171844-2171767/2173039-2171844-2171767.pdf)

---

## Identificación de enfermedades cardiovasculares riesgosas <a name="proy10"></a>

**Autores: Juan Camilo Londoño Jaimes, Hernando José Rojas Castro, María Angélica Serrano Mora**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2172006-2171988-2171988/2172006-2171988-2171988.jpg" style="width:700px;">

**Objetivo: Este proyecto tiene como propósito identificar enfermedades cardiovasculares riesgosas.**

- Dataset: [(Cardiovascular Disease dataset)](https://www.kaggle.com/sulianova/cardiovascular-disease-dataset)
- Modelo: SVC, DecisionTreeClassifier, RandomForestClassifier, GaussianNB, Dense Neural Network.

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2172006-2171988-2171988/2172006-2171988-2171988.ipynb) [(video)](https://www.youtube.com/watch?v=x5tqI0VTIfY) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2172006-2171988-2171988/2172006-2171988-2171988.pdf)-->

---

## Generador FEN de ajedrez <a name="proy11"></a>

**Autores: Cristian Eduardo Rojas Pedraza**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2155505/2155505.jpg" style="width:700px;">

**Objetivo: Generador FEN de ajedrez.**

- Dataset: [(Chess Positions)](https://www.kaggle.com/koryakinp/chess-positions)
- Modelo: DecisionTreeClassifier, SVC, RandomForestClassifier, Convolutional neural network.

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2155505/2155505.ipynb) [(video)](https://www.youtube.com/watch?v=eYV33pP85fs) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2155505/2155505.pdf)-->

---

## Estudio del Covid19 en Colombia <a name="proy12"></a>

**Autores: Sebastian Florez Rojas**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2180050/2180050.jpg" style="width:700px;">

**Objetivo: Estudio del Covid19 en Colombia.**

- Dataset: [(Casos positivos de COVID-19 en Colombia)](https://www.datos.gov.co/en/Salud-y-Protecci-n-Social/Casos-positivos-de-COVID-19-en-Colombia/gt2j-8ykr)
- Modelo: (...)

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2180050/2180050.ipynb) [(video)](https://www.youtube.com/watch?v=GQe9utzbNYA) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2180050/2180050.pdf)-->

---

## Dog Breed Classifier <a name="proy13"></a>

**Autores: Hollman Esteban González Suárez, Kevin Alonso Luna Bustos, Jhoann Sebastián Martínez Oviedo**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2172002-2172022-2171995/2172002-2172022-2171995.jpg" style="width:700px;">

**Objetivo: Clasificar la raza de un perro a partir de una imagen.**

- Dataset: [(Standford Dogs Dataset)](https://www.kaggle.com/c/dog-breed-identification/data)
- Modelo: (...)

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2172002-2172022-2171995/2172002-2172022-2171995.ipynb) [(video)](https://www.youtube.com/watch?v=4A6iO41pjIQ) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2172002-2172022-2171995/2172002-2172022-2171995.pdf)-->

---

## Detección de emociones <a name="proy14"></a>

**Autores: Angie Natalia Arías Gómez, Andrea Sofia Jaimes Alquichire, Juan Camilo Marín García**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2172017-2172716-2172969/2172017-2172716-2172969.jpg" style="width:700px;">

**Objetivo: Detección de emociones.**

- Dataset: [(Datos)](https://github.com/DerekMazino/Inteligencia-Artificial/raw/main/Proyecto-Final-IA-Detector-de-Emociones/dataset.zip)
- Modelo: (...)

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2172017-2172716-2172969/2172017-2172716-2172969.ipynb) [(video)](https://www.youtube.com/watch?v=pwY7xr7dEno) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2172017-2172716-2172969/2172017-2172716-2172969.pdf)--> [(repo)](https://github.com/DerekMazino/Inteligencia-Artificial/tree/main/Proyecto-Final-IA-Detector-de-Emociones)

---

## Clasificación del grado de Alzheimer mediante imágenes <a name="proy15"></a>

**Autores: Juan Felipe Ortiz Trillos, Sergio Andrés Carrilo Muñoz, Diego Andrés Ortega Gelvez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2170105-2171714-2170079/2170105-2171714-2170079.jpg" style="width:700px;">

**Objetivo: Clasificación del grado de Alzheimer mediante imágenes.**

- Dataset: [(Alzheimer's Dataset)](https://www.kaggle.com/tourist55/alzheimers-dataset-4-class-of-images)
- Modelo: (...)

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-2/2170105-2171714-2170079/2170105-2171714-2170079.ipynb) [(video)](https://www.youtube.com/watch?v=90BaEXvkbRg) <!--[(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-2/2170105-2171714-2170079/2170105-2171714-2170079.pdf)--> [(repo)](https://github.com/feltri08/Clasificacion-imagenes-de-alzheimer)

---
